package com.swisscom.health.elements;

import com.erni.testing.elements.TextField;
import com.erni.testing.pages.BasePage;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

public class DatePicker extends TextField {
    public DatePicker(String description, BasePage page) {
        super(description, page);
    }
    public void click() {
        super.click();
    }
    public void click(int waitTimeInSec) {
        super.click(waitTimeInSec);
    }
    public void enterText(String string) {
        super.enterText(string);
    }
    public void enterText(String text, int waitTimeInSec) {
        waitForDocumentReady();
        waitForAjaxReady();
        waitForAnimationsReady();
        WebElement webElement;
        if (this.webElement == null) {
            webElement = waitUntilClickable(waitTimeInSec);
        } else {
            webElement = this.webElement;
        }
//        Doesn't accept webElement.clear();
//        Doesn't accept new Actions(Testing.driver).keyDown(Keys.CONTROL).sendKeys(Keys.chord("A")).keyUp(Keys.CONTROL).perform();
        for (int i=0; i<10; i++) { // There should be no more than ten characters in a date.
            webElement.sendKeys(Keys.BACK_SPACE);
        }
        webElement.sendKeys(text);
    }
}
