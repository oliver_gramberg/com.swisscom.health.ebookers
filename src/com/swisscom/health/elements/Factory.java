package com.swisscom.health.elements;

import com.erni.testing.pages.BasePage;

public class Factory extends com.erni.testing.elements.Factory {
    public DatePicker datePicker(String description) {
        return new DatePicker(description, page);
    }
}
