package com.swisscom.health;

import com.erni.testing.base.Framework;
import org.testng.Assert;

/**
 * Wrapper/adaptor to make framework-specific operations available under general names.
 */
public class TestNG extends Framework {
    public void fail(String message) {
        Assert.fail(message);
    }
}
