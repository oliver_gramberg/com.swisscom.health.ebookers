package com.swisscom.health.tests;

import com.erni.testing.base.Testing;
import com.swisscom.health.TestNG;
import com.swisscom.health.elements.Factory;
import com.swisscom.health.pages.MainPage;
import com.swisscom.health.pages.ResultsPage;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

@Test()
public class TestEbookers {

    @BeforeClass
    public void setup() {

        // Tell that we use TestNG.
        Testing.framework = new TestNG();
        // Tell where our special elements are.
        Testing.elementFactoryClass = Factory.class;
        // If not told otherwise, wait max 3 secs before timing out.
        Testing.defaultWaitTimeInSec = 3;

        // Define webdrivers.
        System.setProperty("webdriver.gecko.driver", "C:\\Users\\gro\\Desktop\\ebookers\\geckodriver.exe");
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\gro\\Desktop\\ebookers\\chromedriver.exe");
        // Start a browser.
        Testing.driver = new ChromeDriver();
    }

    @AfterClass
    public void teardown() {
        Testing.driver.close();
    }

    public void showMostExpensiveFlightsToNycForFamilyOfThree()  {
        MainPage mainPage = new MainPage().goTo();
        mainPage.nurFlugButton.click();
        mainPage.hinUndRückRadioButton.click();
        mainPage.abflughafenTextField.enterText("ZRH");
        mainPage.zielflughafenTextField.enterText("NYC");
        mainPage.datumHinflugTextField.enterText("18.1.2020");
        mainPage.datumRückflugTextField.enterText("25.1.2020");
        mainPage.anzahlErwachseneDropDown.select("2");
        mainPage.anzahlKinderDropDown.select("1");
        mainPage.alterKind1DropDown.select("7");
        mainPage.erweitereOptionenLink.click();
        mainPage.direktflugCheckBox.click();
        mainPage.suchenButton.click();

        ResultsPage resultsPage = ResultsPage.verifyOn();
        resultsPage.sortierenDropDown.select("Preis (absteigend)");
    }
}

