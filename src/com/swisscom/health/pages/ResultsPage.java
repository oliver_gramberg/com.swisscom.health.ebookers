package com.swisscom.health.pages;

import com.erni.testing.elements.BaseElement;
import com.erni.testing.elements.DropDown;
import com.erni.testing.pages.BasePage;

public class ResultsPage extends BasePage {

    // Verify we are on the search results page, and if so return a page object.
    public static ResultsPage verifyOn() {
        ResultsPage resultsPage = new ResultsPage();
        resultsPage.verify();
        return resultsPage;
    }

    // Get a URL to load or verify.
    @Override
    public String getUrl() {
        return "https://www.ebookers.ch/Flights-Search?";
    }

    // Wait for a sign the page has finished loading.
    @Override
    public void waitUntilLoaded() {
        fortschritt.waitUntilPresent(10);
    }

    public BaseElement fortschritt = make.element("Fortschrittsanzeige").onlyByCss(".fill[style='width: 100%;']");
    public DropDown sortierenDropDown = make.dropDown("Sortieren nach").firstByCss("select#sortDropdown");
}
