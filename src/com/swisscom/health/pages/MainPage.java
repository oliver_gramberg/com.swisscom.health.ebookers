package com.swisscom.health.pages;

import com.erni.testing.elements.*;
import com.erni.testing.pages.BasePage;
import com.swisscom.health.elements.Factory;

public class MainPage extends BasePage<Factory> {

    // Get a URL to load or verify.
    @Override
    public String getUrl() {
        return "https://www.ebookers.ch/";
    }

    // Wait for a sign the page has finished loading.
    @Override
    public void waitUntilLoaded() {
        nurFlugButton.waitUntilClickable();
    }

    public Button nurFlugButton = make.button("Flüge").onlyByCss("button#tab-flight-tab-hp");
    public RadioButton hinUndRückRadioButton = make.radioButton("Hin- und Rückflug").onlyByCss("input#flight-type-roundtrip-hp-flight");
    public TextField abflughafenTextField = make.textField("Abflughafen").onlyByCss("input#flight-origin-hp-flight");
    public TextField zielflughafenTextField = make.textField("Zielflughafen").onlyByCss("input#flight-destination-hp-flight");
    public TextField datumHinflugTextField = make.datePicker("Hinflug am").onlyByCss("input#flight-departing-hp-flight");
    public TextField datumRückflugTextField = make.datePicker("Rückflug am").onlyByCss("input#flight-returning-hp-flight");
    public DropDown anzahlErwachseneDropDown = make.dropDown("Erwachsene").onlyByCss("select#flight-adults-hp-flight");
    public DropDown anzahlKinderDropDown = make.dropDown("Kinder").onlyByCss("select#flight-children-hp-flight");
    public DropDown alterKind1DropDown = make.dropDown("Alter von Kind 1").firstByCss("select#flight-age-select-1-hp-flight");
    public Link erweitereOptionenLink = make.link("Erweiterte Optionen").onlyByCss("a#flight-advanced-options-hp-flight");
    public Checkbox direktflugCheckBox = make.checkbox("Direktflug").onlyByCss("input#advanced-flight-nonstop-hp-flight");
    public Button suchenButton = make.button("Suchen").firstByCss(".gcw-submit"); //""form#gcw-flights-form-hp-flight .btn-action.btn-primary.gcw-submit");
}
